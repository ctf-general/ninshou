(ns ninshou.ldap
  (:import com.unboundid.ldap.sdk.LDAPBindException)
  (:require [clj-ldap.client :as ldap]
            [mount.core :refer [defstate]]
            [ninshou.config :refer [config]])
  (:gen-class))

(defn ->UserSummary [ldap-user]
  (->
   ldap-user
   (select-keys [:sAMAccountName :displayName :givenName :sn :mail])
   (clojure.set/rename-keys
    {:sAMAccountName :short-user
     :displayName :display-name
     :givenName :given-name
     :sn :last-name
     :mail :email})))

(defprotocol Authenticate
  (auth-user [conn credentials]))

(def ldap-field-map {:user :sAMAccountName
                     :email :userPrincipalName})

(deftype LdapConn [conn base-dn]
  Authenticate
  (auth-user [conn credentials]
    (let [user-conn (ldap/get-connection (.conn conn))
          user-field (some #{:user :email} (keys credentials))
          username  (if (= user-field :user)
                      (str (:user credentials) "@" (get-in config [:ldap :realm]))
                      (:email credentials))]
      (try
        (when (ldap/bind? user-conn username (:password credentials))
          (->
           (ldap/search user-conn
                        (get-in config [:ldap :base-dn])
                        {:filter (str (name (user-field ldap-field-map)) "=" (user-field credentials))
                         :attributes [:sAMAccountName
                                      :displayName
                                      :givenName
                                      :sn
                                      :mail]})
           first
           ->UserSummary))
        (finally (ldap/release-connection (.conn conn) user-conn))))))

(defn get-conn [ldap-config]
  (let [conn    (ldap/connect (select-keys ldap-config [:host :ssl?]))
        base-dn (:base-dn ldap-config)]
    (LdapConn. conn base-dn)))

(defstate conn :start (get-conn (:ldap config)))
