(ns ninshou.token
  (:require [buddy.sign.jwt :as jwt]
            [buddy.core.keys :as k]
            [clj-time.core :as time]
            [mount.core :refer [defstate]]
            [ninshou.config :refer [config]])
  (:gen-class))

(defn read-private-key [path]
  (k/private-key path))

(defn payload [user-summary]
  {:sub (:short-user user-summary)
   :iat (time/now)
   :iss "ctf::ninshou"})

(defn sign [payload private-key]
  (jwt/sign payload private-key {:alg :es256}))

(defstate private-key :start (read-private-key (get-in config [:jwt :private])))
