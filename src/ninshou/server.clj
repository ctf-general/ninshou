(ns ninshou.server
  (:use ring.adapter.jetty)
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ninshou.ldap :as ldap :refer [auth-user]]
            [ninshou.token :as token]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [ring.util.response :as r]
            [clojure.spec.alpha :as s])
  (:gen-class))


(defn one-of? [m k1 k2]
  (let [has-k1 (contains? m k1)
        has-k2 (contains? m k2)]
  (and (or has-k1 has-k2)
       (not (and has-k1 has-k2)))))

(s/def :credentials/user string?)
(s/def :credentials/email string?)
(s/def :credentials/password string?)
(s/def ::credentials (s/and (s/keys :req-un [(or :credentials/email
                                                 :credentials/user) :credentials/password])
                            #(one-of? % :email :user)))

(defn simple-auth [auth credentials token-key]
  (let [result (auth-user auth credentials)]
    (if-not (nil? result)
      (-> result
          token/payload
          (token/sign token-key)
          r/response
          (r/content-type "text/plain"))
      (-> (r/response "Bad credentials")
          (r/status 401)))))

(defn health-check [ldap-auth]
  (let [rc  (.conn ldap-auth)
        _   (.setTrySynchronousReadDuringHealthCheck rc true)
        hci (.getHealthCheck rc)
        hc  (.invokeHealthCheck rc hci false)
        tot (.getNumExamined hc)
        bad (.getNumDefunct hc)
        res {:healthy (and
                       (not (= tot 0))
                       (= bad 0))
             :connections tot}]
    (if-not (= 0 tot)
      (assoc res :bad-connections-ratio (/ bad tot))
      res)))

(defn api-routes [auth token-key]
  (routes
   (context "/api/v1" []
            (GET "/health" []
                 (-> (r/response (health-check auth))
                     (r/content-type "application/json")))
            (POST "/authenticate/simple" {body :body}
                  (if-not (s/valid? ::credentials body)
                    (r/bad-request (str "Bad payload: "
                                        (s/explain-str ::credentials body)))
                    (simple-auth auth body token-key))))
   (OPTIONS "/" []
            (r/response ""))
   (route/not-found "Not Found")))

(defn cors-allow-all [handler]
  (fn [req]
    (let [response (handler req)]
         (assoc-in response
                   [:headers "Access-Control-Allow-Origin"]
                   "*"))))

(defn start-server [conn config token-key]
  (->
   conn
   (api-routes token-key)
   (wrap-json-body {:keywords? true})
   (wrap-json-response)
   (wrap-cors :access-control-allow-origin [#".*"]
              :access-control-allow-methods [:get :post :options])
   (run-jetty (:server config))))
