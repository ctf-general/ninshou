(ns ninshou.core
  (:require [ninshou.token :as token]
            [ninshou.ldap :as ldap]
            [ninshou.config :refer [config]]
            [ninshou.server :as server]
            [mount.core :as mount])
  (:gen-class))

(defn -main
  [& args]
  (do
    (mount/start)
    (server/start-server ldap/conn config token/private-key)))
