(ns ninshou.config
  (:require [aero.core :refer [read-config]]
            [mount.core :refer [defstate]]
            [clojure.spec.alpha :as s])
  (:gen-class))

(s/def :server/port integer?)
(s/def ::server (s/keys :req-un [:server/port]))

(s/def :jwt/private string?)
(s/def ::jwt (s/keys :req-un [:jwt/private]))

(s/def :ldap/host string?)
(s/def :ldap/realm string?)
(s/def :ldap/base-dn string?)
(s/def :ldap/ssl? boolean?)
(s/def ::ldap (s/keys :req-un [:ldap/host
                               :ldap/realm
                               :ldap/base-dn
                               :ldap/ssl?]))

(s/def ::config (s/keys :req-un [::server ::jwt ::ldap]))

(defn load-config []
  (let [cfg (read-config "config.edn")]
    (if-not (s/valid? ::config cfg)
      (throw (Exception. (str "Config is invalid: "
                              (s/explain-str ::config cfg))))
      cfg)))

(defstate config :start (load-config))

