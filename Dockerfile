FROM clojure:openjdk-11-lein AS builder

COPY . /ninshou/
WORKDIR /ninshou
RUN lein uberjar

FROM openjdk:11

WORKDIR /ninshou
COPY --from=builder /ninshou/target/uberjar/ninshou-*-standalone.jar /ninshou/ninshou.jar
EXPOSE 80
CMD java -jar ninshou.jar