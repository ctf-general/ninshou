# ninshou

Abstraction over authentication.

Simple RESTful service exchanging credentials for JWT tokens after validating with a configurable backend.

## Building

A deployable standalone package can be built with `lein uberjar`

## Generating a keypair

```
# openssl ecparam -name prime256v1 -genkey -noout -out /path/to/private_key.pem
# openssl ec -in /path/to/private_key.pem -pubout -out /path/to/public_key.pem
```

The public key must be shared with all services which wish to verify tokens.
The private key must remain private.

## Configuration

Expects a file named `config.edn` at the root at runtime.

There is sufficient validation to help you create this file with the required entries.

A good start would be something along the lines of

```
{:server {:port 8080}
 :ldap   {:host "ldap.example.com:636"
          :realm "ldap.example.com"
          :base-dn "dc=ldap,dc=example,dc=com"
          :ssl? true}
 :jwt    {:private "/path/to/private_key.pem"}}
```

## API Description
You can `POST` `application/json` to `/api/v1/authenticate/simple` in the form

```
{
  "user": "johnny",
  "password": "lovingEveryLaugh"
}
```
You can also use an email address if your backend supports this.
```
{
  "email": "johnny@example.com",
  "password": "lovingEveryLaugh"
}
```
If all goes well, this will return you a `text/plain` response containing a JWT token
signed with the key specified in the config.

The `sub` field in this token is set to the username of the successfully authenticated user.

The `iat` field is the time at which the token was issued.

The `iss` field is set to `ctf::ninshou`

## License

This software is released under the MIT/X11 license.

A copy is distributed with the source code.
