(defproject ninshou "0.1.0-SNAPSHOT"
  :description "Abstraction over Authentication"
  :url "https://gitlab.science.mcgill.ca/ctf-general/ninshou"
  :license {:name "MIT"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojars.pntblnk/clj-ldap "0.0.16"]
                 [buddy/buddy-sign "3.1.0"]
                 [aero "1.1.3"]
                 [mount "0.1.16"]
                 [clj-time "0.15.2"]
                 [ring-cors "0.1.13"]
                 [ring/ring-core "1.7.1"]
                 [ring/ring-json "0.5.0"]
                 [ring/ring-jetty-adapter "1.7.1"]
                 [compojure "1.6.1"]]
  :main ^:skip-aot ninshou.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
